import ForgeUI, {
  Button,
  ConfigForm,
  Form,
  Fragment,
  Macro,
  Option,
  Select,
  Text,
  TextArea,
  TextField,
  render,
  useAction,
  useConfig,
  useProductContext,
  useState,
  UserPicker,
  RadioGroup,
  Radio,
  ButtonSet,
} from "@forge/ui";
import api from "@forge/api";

const STATE = {
  INITIAL: 0,
  INPUT: 1,
  SUCCESS: 2,
};

const fetchProjects = async () => {
  const response = await api.asApp().requestJira("/rest/api/2/project");
  return response.json();
};

const fetchIssueTypes = async () => {
  const response = await api.asApp().requestJira("/rest/api/2/issuetype");
  const issueTypes = await response.json();
  return issueTypes.filter((issueType) => !issueType.subtask);
};

const Config = () => {
  const [projects] = useState(fetchProjects);
  const [issueTypes] = useState(fetchIssueTypes);

  return (
    <ConfigForm>
      <TextField
        name="ratingButtonText"
        label="Rating Button Text"
        defaultValue="⭐ Rate this content!"
      ></TextField>
      <TextField
        name="ratingFive"
        label="Rating 5 button text"
        defaultValue="Very Happy 😁"
      ></TextField>
      <TextField
        name="ratingFour"
        label="Rating 4 button text"
        defaultValue="Happy 🙂"
      ></TextField>
      <TextField
        name="ratingThree"
        label="Rating 3 button text"
        defaultValue="Ok 😐"
      ></TextField>
      <TextField
        name="ratingTwo"
        label="Rating 2 button text"
        defaultValue="Not Happy 😕"
      ></TextField>
      <TextField
        name="ratingOne"
        label="Rating 1 button text"
        defaultValue="Angry 😡"
      ></TextField>
      <Select label="Project" name="projectKey">
        {projects.map((project) => {
          return (
            <Option
              label={`${project.name} (${project.key})`}
              value={project.key}
            />
          );
        })}
      </Select>
      <Select label="Issue type" name="issueTypeId">
        {issueTypes.map((issueType) => {
          return <Option label={issueType.name} value={issueType.id} />;
        })}
      </Select>
      <UserPicker label="Assign to" name="assignee" />
      <RadioGroup label="Set reporter as" name="reporter">
        <Radio label="Feedback author" value="author" />
        <Radio label="Anonymous" value="anonymous" />
      </RadioGroup>
    </ConfigForm>
  );
};

const App = () => {
  const { accountId } = useProductContext();
  const config = useConfig();
  const [state, setState] = useState(STATE.INITIAL);
  const [error, setError] = useState(null);
  const [issueKey, setIssueKey] = useState(null);

  if (!config) {
    return doNeedConfig();
  }

  switch (state) {
    case STATE.INITIAL:
      return doInitial();
    case STATE.INPUT:
      return doInput();
    case STATE.SUCCESS:
      return doSuccess();
  }

  function doNeedConfig() {
    return <Text content="This app requires configuration before use." />;
  }

  function doInitial() {
    return (
      <Button
        text={config.ratingButtonText}
        onClick={() => {
          setState(STATE.INPUT);
        }}
      />
    );
  }

  function doInput() {
    return (
      <Fragment>
        <ButtonSet>
          <Button text={config.ratingFive} onClick={() => submitReview(5)} />
          <Button text={config.ratingFour} onClick={() => submitReview(4)} />
          <Button text={config.ratingThree} onClick={() => submitReview(3)} />
          <Button text={config.ratingTwo} onClick={() => submitReview(2)} />
          <Button text={config.ratingOne} onClick={() => submitReview(1)} />
        </ButtonSet>
      </Fragment>
    );
  }

  function doSuccess() {
    return (
      <Fragment>
        <Text
          content={`🙏 **Created [${issueKey}](/browse/${issueKey}). Thanks for rating!**`}
        />
      </Fragment>
    );
  }

  async function submitReview(rating) {
    var ratingText;
    if (rating == 5) {
      ratingText = config.ratingFive;
    } else if (rating == 4) {
      ratingText = config.ratingFour;
    } else if (rating == 3) {
      ratingText = config.ratingThree;
    } else if (rating == 2) {
      ratingText = config.ratingTwo;
    } else {
      ratingText = config.ratingOne;
    }

    var summary = "Rating Received: " + " " + ratingText;
    var description = "Rating for confluence page recieved " + ratingText;
    setIssueKey(null);
    const response = await api.asApp().requestJira("/rest/api/2/issue", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        fields: {
          project: {
            key: config.projectKey,
          },
          issuetype: {
            id: config.issueTypeId,
          },
          reporter: {
            id: config.reporter === "author" ? accountId : null,
          },
          assignee: {
            id: config.assignee,
          },
          summary,
          description,
        },
      }),
    });
    const responseBody = await response.json();
    if (!response.ok) {
      console.error(responseBody);
      const errorMessage = responseBody.errorMessages[0];
      setError(errorMessage || "Failed to create issue.");
    } else {
      setError(null);
      setIssueKey(responseBody.key);
      setState(STATE.SUCCESS);
    }
  }
};

export const run = render(<Macro app={<App />} config={<Config />} />);
